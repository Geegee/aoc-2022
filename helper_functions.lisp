(ql:quickload "cl-ppcre")
(defun get-input (fn)
  (let ((in (open  fn :if-does-not-exist nil))
        (res nil))
    (when in
      (loop for line = (read-line in nil)
            while line do (setq res (cons line res)))
      (close in)
      )
    res
    )
  )

(defun split-line (line)
  (cl-ppcre:all-matches-as-strings "[^ ]+" line)
  )

(defun pow (num k &optional (start 1))
  (if (> k 0)
      (pow num (1- k) (* num start))
      start
      )
  )

; crazy function that turns any string into a function
; the real important stuff is (symbol-function (find-symbol "+" *package*))
(defun string->function (name &key (readtable-case (readtable-case *readtable*))
                              (package *package*))
  (let ((effective-name (map 'string
                             (ecase readtable-case
                               ((:upcase)
                                #'char-upcase)
                               ((:downcase)
                                #'char-downcase)
                               ((:preserve)
                                #'identity)
                               ((:invert)
                                (lambda (c)
                                  (cond
                                   ((upper-case-p c)
                                    (char-downcase c))
                                   ((lower-case-p c)
                                    (char-upcase c))
                                   (t c)))))
                             name)))
    (multiple-value-bind (s status) (find-symbol effective-name package)
      (unless status
        (error "no symbol for ~S (from ~S)" effective-name name))
      (unless (fboundp s)
        (error "no function for ~S (from ~S, originally ~S)" s effective-name name))
      (symbol-function s))))
