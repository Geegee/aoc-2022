(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p1_input.txt")
(defparameter *raw-input* (get-input *fn*))
; get total calories of the elf carrying the most calories.
; (integer-max (calories-per-elf *raw-input*))
; get the sum of the highest 3 values
; (apply #'+ (integer-nth-max (calories-per-elf *raw-input*) '(0 0 0)))
(defun get-input (fn)
  (let ((in (open  fn :if-does-not-exist nil))
        (res nil))
    (when in
      (loop for line = (read-line in nil)
            while line do (setq res (cons line res)))
    (close in)
    )
    res
    )
  )

(defun calories-per-elf (raw-input)
  (let ((calorie-list nil) (cur-elf-calories 0))
    (loop for line in raw-input
          do (if (equal line "")
                 (progn
                   (setq calorie-list (cons cur-elf-calories calorie-list))
                   (setq cur-elf-calories 0))
                 (setq cur-elf-calories (+ (parse-integer line) cur-elf-calories))
                 )
          )
    calorie-list
    )
  )

(defun integer-max (lis start-val)
  (if lis
      (let ((head (car lis)) (tail (cdr lis)))
        (if (> head start-val)
            (integer-max tail head)
            (integer-max tail start-val )
        )
      
      )
      start-val
      )
  )

(defun integer-nth-max (lis &optional (input-lis nil))
  ; find the nth highest values in the input list
  ; always need to ask if the current element is higher than
  (if lis
      (integer-nth-max (cdr lis) (sort (put-num-in-lis input-lis (car lis)) #'<))
      input-lis
      )
  )

(defun put-num-in-lis (lis num &optional (rtrn-lis nil))
  (if lis
      (if (> num (car lis))
          (concatenate 'list (cdr lis) (cons num rtrn-lis))
          (put-num-in-lis (cdr lis) num (cons (car lis) rtrn-lis)))
      (reverse rtrn-lis)
      )
  )

(defun find-val-pos (lis val &optional (pos 1))
      (if lis
          (if (not (eq (car lis) val))
              (find-val-pos (cdr lis) val (1+ pos))
              pos
              )
          )
  )


