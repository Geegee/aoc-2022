(ql:quickload "cl-ppcre")
(load "helper_functions.lisp")
(defparameter *raw-example* (uiop:read-file-lines "p10_example.txt"))
(defparameter *raw-input* (uiop:read-file-lines "p10_input.txt"))
(defparameter *example* (mapcar #'(lambda (x) (if (cdr x) (list (car x) (parse-integer (cadr x))) x))
        (mapcar #'split-line *raw-example*)))
(defparameter *input* (mapcar #'(lambda (x) (if (cdr x) (list (car x) (parse-integer (cadr x))) x))
                              (mapcar #'split-line *raw-input*)))
(defparameter *cycle-states* (reverse (process-instr *input*)))


; suspicious to me "maybe you can learn something from the value of the x register throughout execution"
; I guess we want to keep that value throughout execution
; idea is to have one entry per cycle
; We going to keep the state of the register at the start of each cycle
; each cycle has a start and final value.
(defun answer-p1 (cycle-states)
  (+ (* 20 (cdr (nth 19 cycle-states)))
     (* 60 (cdr (nth 59 cycle-states)))
     (* 100 (cdr (nth 99 cycle-states)))
     (* 140 (cdr (nth 139 cycle-states)))
     (* 180 (cdr (nth 179 cycle-states)))
     (* 220 (cdr (nth 219 cycle-states)))
     )
  )


(defun format-crt (crt-symbols crt-width &optional (str-li nil))
  (if crt-symbols
      (if (>= (length crt-symbols) crt-width)
          (let ((head (subseq crt-symbols 0 crt-width)) (tail (subseq crt-symbols crt-width)))
            (format-crt tail crt-width (cons (coerce head 'string) str-li))
            )
          (reverse str-li)
          )
      (reverse str-li)
      )
  )

; if sprite positon and crt-pos overlap append "#" else "."
; we have two more options. Either we move the crt pos back
; or we multiply the register value
(defun draw-crt (cycle-states &optional (output-lis nil) (crt-pos 0) (crt-width 40))
  (if cycle-states
      (let* ((head (car cycle-states)) (tail (cdr cycle-states)) (pixel (draw-pixel crt-pos (cdr head))))
        (draw-crt tail (cons pixel output-lis) (upd-crt-pos crt-pos crt-width))
        )
      (reverse output-lis)
      )
  )

(defun upd-crt-pos (crt-pos &optional (crt-width 40))
  (if (eq crt-pos (1- crt-width))
      0
      (1+ crt-pos))
  )

(defun draw-pixel (crt-pos register)
  (if (overlap (get-sprite register) crt-pos)
      #\#
      #\.)
  )

(defun get-sprite (pos)
  (list (1- pos) pos (1+ pos))
  )

(defun  overlap (sprite crt-pos)
  (if (member crt-pos sprite)
      t
      nil)
  )

(defun process-instr (instructions &optional (state (list (cons 1 1))))
  (if instructions
      (let ((new-register-states (handle-instr (car state) (car instructions))))
        (process-instr (cdr instructions) (concatenate 'list new-register-states state))
        )
      state
      )
  )

(defun handle-instr (register instr)
  (if (equal (car instr) "addx")
      (reverse (list (cons (cdr register) (cdr register)) (cons (cdr register) (+ (cdr register) (cadr instr)))))
      (list (cons (cdr register) (cdr register)))
      )
  )
