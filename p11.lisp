(load "helper_functions.lisp")
(load "/home/geegee/Sync/programming/Projekt Euler/P_3/P_31.lisp")
(defparameter *raw-example* (uiop:read-file-lines "p11_example.txt"))
(defparameter *raw-input* (uiop:read-file-lines "p11_input.txt"))
(defparameter *monkeys* (read-monkeys *raw-example*))
(defparameter *throw-counter* (make-array (length *monkeys*) :initial-element 0))
(defparameter *excluded-primes* '(2 3 5 7 11 13 17 19))
(defparameter *primes* (find-primes-up-to 10000))
; First we need the functionality to read in a monkey
; the next thing is to perform the game.
; that includes going through the items updating their worry level and
; then move the item to a new monkey and appending the item to the new monkeys inventory

; in part 2 we are supposed to simulate a lot of rounds as some update rules include
; multiplication and therefore to exponential growth of the item number.

; Idea 1
; We use the fact that adding to a number will either destroy the divisibility
; of a prime number or if we add a prime number then it will keep it.

; We multiply by prime numbers or square the number
; In one case we need to add divisibility by the prime number to the item
; in the other we keep the divisibility unchanged.
; In case of adding the situation is a lot more complicated. The problem is
; that if I do not 


(defun do-monkey-rounds (num monkeys)
  (loop for round from 0 to (1- num)
        do (monkey-round monkeys)
        )
  )

(defun monkey-round (monkeys)
  (loop for monkey in monkeys for ind from 0 to (1- (length monkeys))
        do (monkey-turn ind monkeys)
        )
  )

(defun monkey-turn (num monkeys)
  (let* ((pivot-monkey (nth num monkeys)) (items (first pivot-monkey)) (upd-fun (second pivot-monkey))
         (div-test (nth 2 pivot-monkey)) (t-if (nth 3 pivot-monkey)) (t-if-not (nth 4 pivot-monkey)))
    (process-item-list items upd-fun div-test t-if t-if-not monkeys)
    (remove-monkey-items pivot-monkey)
    (setf (aref *throw-counter* num) (+ (aref *throw-counter* num) (length items)))
    )
  )

; at the end the monkey has thrown all his items away
; to test
(defun process-item-list (items upd-fun div-test t-if t-if-not monkeys)
  (if items
      (let ((head (car items)) (tail (cdr items)))
        (process-item-list tail upd-fun div-test t-if t-if-not
                         (process-item head upd-fun div-test t-if t-if-not monkeys))
        )
      monkeys
      )
  )

; to test
(defun process-item (item upd-fun div-test t-if t-if-not monkeys)
  (let ((new-worry-level (relax (funcall upd-fun item))) (recv-monk0 (nth t-if monkeys))
        (recv-monk1 (nth t-if-not monkeys)))
    (if (funcall div-test new-worry-level)
        (setf (nth 0 recv-monk0) (give-item new-worry-level (nth 0 recv-monk0)))
        (setf (nth 0 recv-monk1) (give-item new-worry-level (nth 0 recv-monk1)))
        )
    monkeys
    )
  )

(defun remove-monkey-items (monkey)
  (setf (car monkey) nil)
  )
; return a list of monkeys
(defun read-monkeys (lines &optional (monkeys nil))
  (if lines
      (let ((head (car lines)) (tail (cdr lines)))
        (if (monkey-line head)
            (read-monkeys (subseq tail 5) (cons (read-monkey (subseq tail 0 5)) monkeys))
            (read-monkeys tail monkeys)
            )
        )
      (reverse monkeys)
      )
  )

(defun monkey-line (line)
  (if (equal (first (split-line line)) "Monkey")
      t
      )
  )

; return a list representation of a monkey
(defun read-monkey (lines &optional (line-num 0) (monkey nil))
    (case line-num
      (0 (read-monkey (cdr lines) (1+ line-num) (cons (read-starting-items (car lines)) monkey)))
      (1 (read-monkey (cdr lines) (1+ line-num) (cons (read-operation (car lines)) monkey)))
      (2 (read-monkey (cdr lines) (1+ line-num) (cons (read-test (car lines)) monkey)))
      (3 (read-monkey (cdr lines) (1+ line-num) (cons (throw-to (car lines)) monkey)))
      (4 (read-monkey (cdr lines) (1+ line-num) (cons (throw-to (car lines)) monkey)))
      (5 (reverse monkey))
  )
)

; return a list of starting items
(defun read-starting-items (line)
  (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings "[0-9]+" line))
  )

; should return a function which takes as input a integer and returns an integer
(defun read-worry-update (line)
  (let* ((tmp (extract-operation-line line)) (op (car tmp)) (var0 (cadr tmp)) (var1 (caddr tmp)))
    (create-operation op var0 var1)
    )
  )

(defun read-operation (line)
  (let* ((tmp (extract-operation-line line)) (op (car tmp)) (var0 (cadr tmp)) (var1 (caddr tmp)))
    (cond ((and (equal var0 "old") (equal var1 "old"))
           (lambda (x) (funcall (read-from-string op) x x)))
          (t
           (lambda (x) (funcall (read-from-string op) x (parse-integer var1)))
           )
          )
    )
  )

; return a function that takes as input a value and returns a boolean (nil or t)
(defun read-test (line)
  (let ((input (parse-integer (car (last (split-line line))))))
    (lambda (x) (eq (mod x input) 0))
    )
  )

; return a value (which monkey to throw to)
(defun throw-to (line)
  (parse-integer (car (last (split-line line))))
  )
; return a value (which monkey to throw to)
(defun give-item (item cur-items)
  (append cur-items (list item))
  )

(defun remove-item (cur-items)
  (cdr cur-items)
  )

(defun relax (item)
  (multiple-value-bind (f) 
      (floor item 3)
    f)
  )

; the idea is that the divisbility rule  consists only of primes
; now if we divide out the primes not occuring in the worry level we reduce
; the size of the number by a lot. (Essentially keeping it smaller than (* 2 3 5 7 11 13 17))
; the question to me is if the updates to the worry level screw up this method 
(defun compress-worry-level (worry-level primes exclude)
  (let ((decomp (prime-decomposition worry-level primes))
        (masked-primes (remove-if #'(lambda (x) (member x exclude)) primes)))
    (apply #'* (mapcar #'(lambda (x) (pow (car x) (cdr x)))
                       (remove-if #'(lambda (x) (member (car x) masked-primes)) decomp)))
    )
  )

(defun extract-operation-line (line)
  (let ((sseq (subseq (split-line line) 3)))
    (list (cadr sseq) (car sseq) (caddr sseq))
    )
  )


; Want to have a macro that allows returning a function.
(defmacro create-operation (op el0 el1)
    (cond ((and (equal el0 "old") (equal el1 "old"))
           (let ((fun-name (gensym)) (var0 (gensym)))
             `(defun ,fun-name (,var0)
                (funcall (string->function ,op) ,var0 ,var0)
                )
             )
           )
          (t
           (let ((fun-name (gensym)) (var0 (gensym)))
             `(defun ,fun-name (,var0)
                (funcall (string->function ,op) ,var0 (parse-integer ,el1))
                )
             )
           )
          )
  )
