(load "p1.lisp")
(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p2_input.txt")
(defparameter *raw-input* (get-input *fn*))
(defparameter *rps-ev-arr* (make-array '(3 3) 
                                       :initial-contents '((3 0 6 ) (6 3 0) (0 6 3))))
(defparameter *moves-lis* (build-cons-lis (elements-to-lis (get-input *fn*))))
; non functions
; return total score
(apply #'+ (mapcar #'(lambda (mov) (points-for-round mov *rps-ev-arr*)) *moves-lis*))
; part 2
(apply #'+ (mapcar #'(lambda (action-key) (score-move-part2 (cdr action-key) (car action-key))) *moves-lis*))
; first we transform the input into a hash table

(defun coerce-to-lis (input)
  (coerce input 'list)
  )

(defun elements-to-lis (input)
  (mapcar #'coerce-to-lis input))

(defun build-cons-lis (input &optional (out-lis nil))
  (if input
      (let ((head (car input)) (tail (cdr input)))
        (build-cons-lis tail (cons (cons (en-mov head) (my-mov head)) out-lis))
        )
      out-lis
      ))

(defun en-mov (input)
  (car input)
  )

(defun my-mov (input)
  (caddr input)
  )
; evaluate round
(defun points-for-round (mov ev-arr)
  (+ (points-for-action (cdr mov)) (points-for-mov mov ev-arr))
  )

(defun points-for-mov (mov ev-arr)
  (let ((fst-ind (action-to-ind (cdr mov))) (snd-ind (action-to-ind (car mov))))
        (aref ev-arr fst-ind snd-ind)
    )
  )

(defun action-to-ind (action)
  (cond ((eq #\X action) 0)
        ((eq #\Y action) 1)
        ((eq #\Z action) 2)
        ((eq #\A action) 0)
        ((eq #\B action) 1)
        ((eq #\C action) 2)
        )
  )

(defun points-for-action (action)
  (cond ((eq #\X action) 1)
        ((eq #\Y action) 2)
        ((eq #\Z action) 3)
        )
  )

; translate key and enemy action to my action and score it
(defun score-move-part2 (key action)
  (cond ((eq key #\X)
         (cond ((eq action #\A) 3)
               ((eq action #\B) 1)
               ((eq action #\C) 2)
               ))
        ((eq key #\Y)
         (cond ((eq action #\A) 4)
               ((eq action #\B) 5)
               ((eq action #\C) 6)
               ))
        ((eq key #\Z)
         (cond ((eq action #\A) 8)
               ((eq action #\B) 9)
               ((eq action #\C) 7)
               ))
        )
  )


