(load "p1.lisp")
(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p3_input.txt")
(defparameter *raw-input* (get-input *fn*))
(defparameter *raw-input-li* (mapcar #'(lambda (x) (coerce x 'list)) *raw-input*))
(defparameter *compartments* (mapcar #'divide-in-compartments *raw-input*))
(defparameter *lis-compartments* (mapcar #'transform-str-bag-to-lis *compartments*)
(defparameter *common-items* (mapcar #'(lambda (x) (common-item (car x) (cdr x))) *li-compartments*))
(defparameter *res-part1* (apply #'+ (mapcar #'item-to-score *common-items*)))
; part 2
(defparameter *raw-input-li-in3* (shape-lis-in-n *raw-input-li* 3))
(defparameter *res-part2* (apply #'+ (mapcar #'(lambda (x) (item-to-score (car (find-common-among-three x)))) *raw-input-li-in3*)))
; get compartments from string
; find item in both compartments
; rate based on item
(defun divide-in-compartments (input)
  (let* ((inp-len (length input)) (dist-to-mid (car (multiple-value-list (floor inp-len 2)))))
    (cons (subseq input 0 dist-to-mid) (subseq input dist-to-mid))
    )
  )

; find common item
(defun common-item (pivot-comp other-comp)
  (if pivot-comp
      (let ((head (car pivot-comp)) (tail (cdr pivot-comp)))
        (if (member head other-comp)
            head
            (common-item tail other-comp))
        )
      )
  )

(defun  transform-str-bag-to-lis (bag)
  (cons (coerce (car bag) 'list) (coerce (cdr bag) 'list))
  )

; map char to score
(defun item-to-score (item)
  (if (upper-case-p item)
      (- (char-int item) 38)
      (- (char-int item) 96)
      )
  )

; find common item among 3 lists
; first find all common items among 2
; then see which of these are in 3
(defun find-common-among-three (three)
  (let ((one (car three)) (two (cadr three)) (three (caddr three)))
    (filter-members (find-all-common-items one two) three)
  )
)

(defun find-all-common-items (pivot-comp other-comp &optional (res nil) (dup nil))
  (if pivot-comp
      (let ((head (car pivot-comp)) (tail (cdr pivot-comp)))
        (if (and (member head other-comp) (not (member head dup)))
            (find-all-common-items tail other-comp (cons head res) (cons head dup))
            (find-all-common-items tail other-comp res dup)
        )
      )
      res
      )
  )

(defun filter-members (members lis &optional (res nil))
  (if members
      (if (member (car members) lis)
          (filter-members (cdr members) lis (cons (car members) res))
          (filter-members (cdr members) lis res)
          )
      res
      )
  )

(defun shape-lis-in-n (lis num &optional (res nil) (tmp nil))
  (if lis
      (if (< (length tmp) num)
          (shape-lis-in-n (cdr lis) num res (cons (car lis) tmp))
          (shape-lis-in-n (cdr lis) num (cons (reverse tmp) res) (list (car lis)))
          )
      (reverse (cons tmp res))
  )
)
