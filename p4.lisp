(load "p1.lisp")
(ql:quickload "cl-ppcre")
(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p4_input.txt")
(defparameter *raw-input* (get-input *fn*))
; part 1
(defparameter *split-input* (mapcar #'extract-ranges-from *raw-input*))
(defparameter *res-p1* (apply #'+ (mapcar #'overlap-p *split-input*)))
; part 2
(defparameter *overlap-filtered* (filter-overlaps *split-input*))
(defparameter *res-p2* (+ (apply #'+ (mapcar #'(lambda (x) (if x 0 1)) (mapcar #'disjunkt-p *overlap-filtered*)))
                          *res-p1*))
; extract integers from string
(defun extract-ranges-from (pair)
  (let* ((e12 (cl-ppcre:split "," pair)) (e1 (car e12)) (e2 (cadr e12)))
    (list (mapcar #'parse-integer (cl-ppcre:split "-" e1))
          (mapcar #'parse-integer (cl-ppcre:split "-" e2))) 
    )
  )
; See if we can get the overlap
; First compare the first number of both pairs.
; The second number of the pair with the first number larger
; must be smaller or equal to the second number of the other pair
(defun filter-overlaps (pairs &optional (filter-lis nil))
  (if pairs
      (let ((head (car pairs)) (tail (cdr pairs)))
        (if (overlap-p head (list T nil))
            (filter-overlaps tail filter-lis)
            (filter-overlaps tail (cons head filter-lis))
            )
         )
      filter-lis
      )
  )

(defun filter-lis (pairs fun &optional (filter-lis nil))
  (if pairs
      (let ((head (car pairs)) (tail (cdr pairs)))
        (if (funcall fun head)
            (filter-overlaps tail filter-lis)
            (filter-overlaps tail (cons head filter-lis))
            )
        )
      filter-lis
      )
  )


(defun disjunkt-p (pairs)
  (let ((a (car (get-first-els pairs))) (c (cadr (get-first-els pairs)))
        (b (car (get-second-els pairs))) (d (cadr (get-second-els pairs))))
    (if (or (< b c) (< d a))
           T))
    )

(defun overlap-p (pairs &optional (rtrn '(1 0)))
  (let ((firsts (get-first-els pairs)) (seconds (get-second-els pairs)))
    (cond ((and (> (car firsts) (cadr firsts))
           (<= (car seconds) (cadr seconds)))
           (car rtrn))
          ((and (< (car firsts) (cadr firsts)) (>= (car seconds) (cadr seconds)))
           (car rtrn))
          ((eq (car firsts) (cadr firsts))
           (car rtrn))
          (T
            (cadr rtrn))
          )
    )
  )

(defun get-first-els (pairs)
  (list (caar pairs) (caadr pairs))
  )

(defun get-second-els (pairs)
  (list (cadar pairs) (cadadr pairs))
  )
