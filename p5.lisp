(load "p1.lisp")
(ql:quickload "cl-ppcre")
(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p5_input.txt")
(defparameter *raw-input* (get-input *fn*))
(defparameter *instr-stack* (split-li *raw-input* 503))
(defparameter *stack-string* (cdr *instr-stack*))
(defparameter *instr-string-dirt* (car *instr-stack*))
(defparameter *instr-string* (reverse (cddr (reverse *instr-string-dirt*))))
(defparameter *instructions* (reverse (mapcar #'get-move-info *instr-string*)))
(defparameter *initial-stacks* (reverse-rem-nil-stacks (get-stacks (collect-stack-lines *stack-string* 9) 9)))
; part 1
(loop for instr in *instructions*
      do (progn
           (do-move *initial-stacks* (car instr) (1- (cadr instr)) (1- (caddr instr)))
           )
      )

(loop for stack in *initial-stacks*
      collect (car stack))
; part 2
(loop for instr in *instructions*
      do (progn
           (do-move-mult *initial-stacks* (car instr) (1- (cadr instr)) (1- (caddr instr)))
           ;(print *initial-stacks*)
           ;(print instr)
           )
      )

(loop for stack in *initial-stacks*
      collect (car stack))

; divide the input into two pa  s
(defun split-li (input where &optional (acc nil))
  (if input
      (if (>= where 0)
          (split-li (cdr input) (1- where) (cons (car input) acc))
          (cons (reverse acc) input)
          )
      (cons (reverse acc) input)
      )
  )

(defun pop-off-nil (stack)
  (if stack
      (if (eq (car stack) nil)
          (progn
            (pop stack)
            (pop-off-nil stack))
          stack)
  )
  )

(defun reverse-rem-nil-stacks (stacks)
  (loop for stack in stacks
        collect (pop-off-nil (reverse stack))))

(defun get-stacks (lines stacks)
  (loop for ii from 0 to (1- stacks)
        collect (built-stack lines ii)
        )
  )
; need to reverse the result list and remove the nils
(defun built-stack (lines stack)
  (loop for line in lines
        collect (nth stack line)))

(defun collect-stack-lines (lines num-stack-els)
  (loop for line in lines
        collect (get-stack-line line num-stack-els))
  )

(defun get-stack-line (line num-stack-els)
  (loop for ii from 0 to (1- num-stack-els)
        collect (get-stack-el line ii))
  )

(defun get-stack-el (line stack)
  (let ((el (string-trim '(#\[ #\] #\Space) (subseq line (* 4 stack) (+ (* 4 stack) 3)))))
    (if (equal el "")
        nil
        el)
    )
  )

(defun pop-stack (stack)
  (car stack)
  )

(defun push-stack (stack el)
  (cons el stack)
   )

(defun do-move-mult (stacks num pos-rem pos-add)
  (let* ((p12 (split-li (nth pos-rem stacks) (1- num))) (mv (car p12)) (stay (cdr p12)))
    (progn
      (setf (nth pos-rem stacks) stay)
      (setf (nth pos-add stacks) (concatenate 'list mv (nth pos-add stacks)))
      )
    )
  )

(defun do-move (stacks num pos-rem pos-add)
  (loop for ii from 0 to (1- num)
        do (if (nth pos-rem stacks)
                (setf (nth pos-add stacks) (cons (pop (nth pos-rem stacks)) (nth pos-add stacks)))
                (print "empty stack")
            )
        )
  )

(defun get-move-info (input)
  (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings "[0-9]+" input))
  )
