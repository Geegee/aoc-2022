(load "p1.lisp")
(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p6_input.txt")
(defparameter *raw-input* (get-input *fn*))
(defparameter *input-str* (car *raw-input*))
(defparameter *input-li* (coerce *input-str* 'list))
(defparameter *example1* (coerce "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw" 'list))
(defparameter *example2* (coerce "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg" 'list))
; 
(defun traverse-list (lis &optional (counter 4) (unique-len 4))
  (if lis
      (let ((ssq (subseq lis 0 unique-len)) (tail (cdr lis)))
        (if (seq-unique ssq)
            (progn (print ssq)
                   counter)
            (traverse-list tail (1+ counter) unique-len)
            )
        )
      counter)
  )

(defun seq-unique (seq)
  ; check if sequence has only unique items
  (if seq
      (let ((head (car seq)) (tail (cdr seq)))
        (if (member head tail :test 'equal)
            nil
            (seq-unique tail)
            )
        )
      t
      )
  )
