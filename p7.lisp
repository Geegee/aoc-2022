(ql:quickload "cl-ppcre")
(load "p1.lisp")
(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p7_input.txt")
(defparameter *raw-input* (reverse (get-input *fn*)))
; some state variables that will be mutated
(defparameter *dirs* '(("/")))
(defparameter *cur-dir* nil)
(defparameter *dir-sizes* '(0))
(defparameter *files* nil)
(defparameter *tmp* nil)
(defparameter *counter* 0)
(defparameter *sys-size* 70000000)
(defparameter *upgrade-size* 30000000)
; we have commands, directories and files
; we need to know in which directories we are and keep book
; need to make sure to not count files twice but a file can be in multiple folders 

(defun print-state ()
  (format t "dirs: ~a ~%cur-dir: ~a ~%dir-sizes: ~a ~%" *dirs* *cur-dir* *dir-sizes*)
  )

(loop for line in *raw-input*
      do (progn
           ; (print-state)
           (print *counter*)
           (setf *counter* (1+ *counter*))
           (setf *tmp* (handle-line line *cur-dir* *dirs* *dir-sizes* *files*))
           (setf *cur-dir* (car *tmp*))
           (setf *dirs* (cadr *tmp*))
           (setf *dir-sizes* (caddr *tmp*))
           (setf *files* (cadddr *tmp*))
           )
      )

(defun add-if-lt (input lt &optional (acc 0))
  (if input
      (if (<= (car input) lt)
          (add-if-lt (cdr input) lt (+ acc (car input)))
          (add-if-lt (cdr input) lt acc))
      acc)
  )

(defun pos-lst (lis st &optional (cur-pos -1) (max-pos 0) (cur-max 0))
  (if lis
      (if (<= (car lis) st)
          (if (> (car lis) cur-max)
              (pos-lst (cdr lis) st (1+ cur-pos) (1+ cur-pos) (car lis))
              (pos-lst (cdr lis) st (1+ cur-pos) max-pos cur-max))
              )
          (pos-lst (cdr lis) st (1+ cur-pos) max-pos cur-max)
      )
      (cons cur-max max-pos)
  )

(defun handle-line (line cur-dir dirs dir-sizes files-visited)
  ; first split the line
  ; then act depending on input
  (let ((sp-line (split-line line)))
    (cond ((is-cd sp-line)
           (setf cur-dir (handle-cd cur-dir (caddr sp-line)))
           )
          ((is-dir sp-line)
           (let ((res (update-dir-list (cadr sp-line) cur-dir dirs dir-sizes)))
             (progn
               (setf dirs (car res))
               (setf dir-sizes (cdr res)))
             )
           )
          ;the assumption is that if we have not ls , cd or dir in the line, then it must be a file
          ((not (is-ls sp-line))
           (progn
             (loop for dir in (cons cur-dir dirs)
                   do (if (and (or (eq dir cur-dir) (dir-contained cur-dir dir))
                                   (not (member (cons (cadr sp-line) cur-dir) files-visited)))
                            (setf dir-sizes
                                  (add-dir-size (parse-integer (car sp-line)) (pos-in-lis dir dirs) dir-sizes)))
                        )
             (setf files-visited (add-file-visited (cons (cadr sp-line) cur-dir) files-visited))
           )
          )
          )
    )
  (list cur-dir dirs dir-sizes files-visited files-visited)
  )

(defun is-command (sp-line)
  (if (equal (car sp-line) "$")
      t)
  )

(defun is-cd (sp-line)
  (if (equal (cadr sp-line) "cd")
      t)
  )

(defun is-ls (sp-line)
  (if (equal (cadr sp-line) "ls")
      t)
  )

(defun is-dir (sp-line)
  (if (equal (car sp-line) "dir")
      t)
  )

(defun split-line (line)
  (cl-ppcre:all-matches-as-strings "[^ ]+" line)
  )

(defun handle-cd (cur-dir arg)
  ; i assume we don't blindly change in a directory
  ; otherwise we would need to update dirs
  (if (equal ".." arg)
         (cdr cur-dir)
         (cons arg cur-dir)
  )
  )

; the problem is that directories could be not unique
; There might be a /home/geegee/tmp and /home/geegee/hello/tmp
; I guess an easy fix is to save the whole path which is unique 
(defun update-dir-list (cand cur-dir dirs dir-sizes)
  (if (not (member cand dirs :test 'equal))
      (progn 
        (setf dirs (cons (cons cand cur-dir) dirs))
        (setf dir-sizes (cons 0 dir-sizes))
      )
      )
  (cons dirs dir-sizes)
  )

(defun pos-in-lis (el lis &optional (pos 0))
  (if lis
      (if (equal el (car lis))
          pos
          (pos-in-lis el (cdr lis) (1+ pos))
          ))
  )

(defun update-dir-size (val dir-pos dir-sizes)
  (setf (nth dir-pos dir-sizes) val)
  )

(defun add-dir-size (val dir-pos dir-sizes)
  (setf (nth dir-pos dir-sizes) (+ val (nth dir-pos dir-sizes)))
  dir-sizes
  )

(defun add-file-visited (file files-visited)
  (if (not (member file files-visited :test 'equal))
      (cons file files-visited)
      files-visited)
  )

(defun get-dir-size (dir-pos dir-sizes)
  (nth dir-pos dir-sizes))

(defun dir-contained (dir-a dir-b)
  ; check if dir-a is contained in dir-b
  (if dir-a
      (if (equal (cdr dir-a) dir-b)
          t
          (dir-contained (cdr dir-a) dir-b))
      nil)
  )

