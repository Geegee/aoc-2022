(load "p1.lisp")
(ql:quickload  "cl-slice")
(defparameter *fn* "/home/geegee/org/roam/Programming/AoC_2022/p8_input.txt")
(defparameter *raw-input* (reverse (get-input *fn*)))
(defparameter *dimensions* '(99 99))
(defparameter *input-mat*  (make-array *dimensions* :initial-contents
                                       (mapcar #'(lambda (x) (mapcar #'digit-char-p (coerce x 'list))) *raw-input*)))

; each tree can be visible from top, bot, left or right
; therefore we make an array with dimensions '(99 99 4) where the encoding is top bot left right
; and 0 means invisible and 1 visible (should be more efficient because most tree presumably invisible)
(defparameter *visi-mat* (make-array '(99 99 4) :initial-element nil))
(defparameter *tree-view* (make-array '(99 99 4) :initial-element 0))
(defparameter *scenic-score* (make-array '(99 99) :initial-element 0))

(defun count-visible (vis-arr dims)
  (let ((tc 0))
    (loop for ii from 0 to (1- (first dims))
          do (loop for jj from 0 to (1- (second dims))
                   do (if (any-true (cl-slice:slice vis-arr ii jj (cons 0 nil)))
                          (setq tc (1+ tc))
                          )))
    tc
    )
  )

(defun max-twod-arr (arr dims)
  (let ((cur-max 0))
    (loop for ii from 0 to (1- (first dims))
          do (loop for jj from 0 to (1- (second dims))
                   do (if (> (aref arr ii jj) cur-max)
                          (progn
                            (format t "~a ~a ~%" ii jj)
                            (setq cur-max (aref arr ii jj))))
                 )
          )
    cur-max
    )
  )

(defun calc-scenic-score (tree-view-num-mat scenic-score-mat dims)
  (loop for ii from 0 to (1- (first dims))
        do (loop for jj from 0 to (1- (second dims))
                 do (setf (aref scenic-score-mat ii jj)
                          (apply #'* (coerce (cl-slice:slice tree-view-num-mat ii jj (cons 0 nil))
                                                           'list)))
                 )
        )
  )

(defun set-all-visibility (vis-arr input-arr dims)
  ; loop through the directions and the dimensions
  (loop for dir from 1 to 2
        do (loop for col from 0 to (1- (second dims))
                 do (set-visibility vis-arr input-arr dir col dims)))
  (loop for dir from 3 to 4
        do (loop for row from 0 to (1- (first dims))
                 do (set-visibility vis-arr input-arr dir row dims)))
  )

(defun set-visibility (vis-arr input-arr dir ind dims)
  (case dir
    (1 (loop for ii in (vis-indices (get-slice input-arr 1 ind))
             do (setf (aref vis-arr ii ind 0) t)))
    (2 (loop for ii in (turn-around-ind (vis-indices (get-slice input-arr 2 ind)) (first dims))
              do (setf (aref vis-arr ii ind 1) t)))
    (3 (loop for ii in (vis-indices (get-slice input-arr 3 ind))
             do (setf (aref vis-arr ind ii 2) t)))
    (4 (loop for ii in (turn-around-ind (vis-indices (get-slice input-arr 4 ind)) (second dims))
             do (setf (aref vis-arr ind ii 3) t))
     )
    )
  )

; when moving through from the other side we need to translate the indices w
(defun turn-around-ind (indices dim)
  (mapcar #'(lambda (x) (- (1- dim) x)) indices)
  )
; move along and find the visible indices 
(defun vis-indices (arr)
  (let ((cur-max -1) (vis-pos nil))
    (loop for ii from 0 to (1- (length arr))
          do (if (> (aref arr ii) cur-max)
              (progn
                (setq cur-max (aref arr ii))
                (setf vis-pos (cons ii vis-pos))))
          )
    vis-pos
    )
  )

; certain combinations are not healthy like dir: 3 row:0 (moving up on row 0 is not working)
; dir:1 row:(1- dim) dir:2 col: (1- dim) dir:4 col:0 need to catch these cases
(defun all-num-trees-vis (arr num-tree-arr dims)
  (loop for dir from 1 to 4
        do (loop for row from 0 to (1- (first dims))
                 do (loop for col from 0 to (1- (second dims))
                          do (progn
                              (format t "~a ~a ~a ~%" dir row col)
                              (if (not (move-out-of-border dir row col dims))
                                 (setf (aref num-tree-arr row col (1- dir)) (num-trees-vis arr (list row col) dir)))
                                 )
                          )
                 )
        )
  )

(defun move-out-of-border (dir row col dims)
  (case dir
    (1 (eq row (1- (first dims))))
    (2 (eq row 0))
    (3 (eq col (1- (second dims))))
    (4 (eq col 0)))
  )

; get the right slice and count the trees
(defun num-trees-vis (arr tree dir)
  (let ((th (aref arr (first tree) (second tree))))
    (case dir
      (1 (count-trees (cl-slice:slice arr (cons (1+ (first tree)) nil) (second tree)) th))
      (2 (count-trees (reverse (cl-slice:slice arr (cons 0 (first tree)) (second tree))) th))
      (3 (count-trees (cl-slice:slice arr (first tree) (cons (1+ (second tree)) nil)) th))
      (4 (count-trees (reverse (cl-slice:slice arr (first tree) (cons 0 (second tree)))) th)))
    )
  )

(defun count-trees (arr val)
  (let ((counter 0) (end-reached t))
    (loop for el across arr
          do (if (> val el)
                 (setq counter (1+ counter))
                 (return (setq end-reached nil))))
    (if end-reached
        counter
        (1+ counter))
    )
  )

(defun get-slice (arr dir ind)
  (case dir
    (1 (cl-slice:slice arr (cons 0 nil) ind))
    (2 (reverse (cl-slice:slice arr (cons 0 nil) ind)))
    (3 (cl-slice:slice arr ind (cons 0 nil)))
    (4 (reverse (cl-slice:slice arr ind (cons 0 nil))))
    )
  )

(defun any-true (arr)
  (let ((at nil))
    (loop for ii from 0 to (1- (length arr))
          do (if (aref arr ii)
                 (setq at t))
          )
    at
    )
  )
