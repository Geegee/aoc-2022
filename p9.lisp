(load "helper_functions.lisp")
(defparameter *source*  "https://adventofcode.com/2022/day/9/input")
(defparameter *fn* "/home/wanderer/org-roam/Programming/AoC_2022/p9_input.txt")
(defparameter *raw-input* (reverse (get-input *fn*)))
(defparameter *mov-comm*
  (mapcar #'(lambda (x) (cons (car x) (parse-integer (cadr x)))) (mapcar #'split-line *raw-input*)))
(defparameter *init-knots* (repeat '(0 . 0) 10))
; the head moves around according to the instructions in the .txt file
; the tail follows.
; The simplest way to me is to keep coordinates we visited in a list and start with (0,0)


(defun process-moves (head-pos tail-pos moves &optional (tail-visits nil))
  (if moves
      (let* ((mov (car moves)) (upd-pos (process-cmd head-pos tail-pos tail-visits (car mov) (cdr mov))))
        (process-moves (car upd-pos) (cadr upd-pos) (cdr moves) (caddr upd-pos)))
      tail-visits
      )
  )

(defun process-cmd (head-pos tail-pos tail-visits dir mov)
  ; (format t "~a ~a ~%" head-pos tail-pos)
  (if (> mov 0)
      (let* ((upd-hdps (update-head-pos head-pos dir))
             (new-tail (if (tail-attached upd-hdps tail-pos)
                           tail-pos
                           (update-tail-pos tail-pos upd-hdps)
                           )))
        (process-cmd upd-hdps
                     new-tail
                     (add-visited new-tail tail-visits)
                     dir
                     (1- mov)))
      (list head-pos tail-pos tail-visits)
      )
  )

(defun process-moves-mult (knots moves &optional (tail-visits nil))
  (if moves
      (let* ((mov (car moves)) (upd-pos (process-cmd-mult knots tail-visits (car mov) (cdr mov))))
        (process-moves-mult (car upd-pos)  (cdr moves) (cdr upd-pos)))
      tail-visits
      )
  )

; in each step we first update the head and then update the knots afterwards
(defun process-cmd-mult (knots tail-visits dir mov)
  ;(format t "~a ~%" knots)
  (if (> mov 0)
      (let* ((head (car knots))
             (tail (cdr knots))
             (upd-hdps (update-head-pos head dir))
             (new-rope (update-knots (cons upd-hdps tail) (list upd-hdps))))
        (process-cmd-mult new-rope (add-visited (car (last new-rope)) tail-visits) dir (1- mov))
        )
      (cons knots tail-visits)
      )
  )

; after the head knot changed according to external forces we need to update the rest accordingly
; I wonder: Probably if for some knot I don't have to update the rest knots
; also won't change their position, but just a guess. 
(defun update-knots (knots &optional (upd-knots nil))
  ; (format t "~a ~%" knots)
  (if (cdr knots)
      (let ((tail (cdr knots)))
        (update-knots (cdr knots) (cons (update-tail-pos-if (car tail) (car upd-knots)) upd-knots))
        )
      (reverse upd-knots)
      ; (reverse (list (cdr knots) upd-knots))
      )
  )

(defun update-head-pos (pos dir)
  (string-case dir
   ("L" (cons (1- (car pos)) (cdr pos)))
   ("R" (cons (1+ (car pos)) (cdr pos)))
   ("D" (cons (car pos) (1- (cdr pos))))
   ("U" (cons (car pos) (1+ (cdr pos))))
   )
  )

(defun update-tail-pos-if (tail-pos head-pos)
  (if (tail-attached head-pos tail-pos)
      tail-pos
      (update-tail-pos tail-pos head-pos)
      )
  )

(defun update-tail-pos (pos head-pos)
  (let ((dif (diff-vec pos head-pos)))
    (cons (update-coord (car pos) (car dif)) (update-coord (cdr pos) (cdr dif)))
    )
  )

(defun update-coord (poscomp difcomp)
  (cond ((> difcomp 0) (1+ poscomp))
        ((< difcomp 0) (1- poscomp))
        (t poscomp)
        )
  )

; tail attached either if they coincide or are one apart
(defun tail-attached (head-pos tail-pos)
  (let ((dif (diff-vec tail-pos head-pos)))
    (if (< (vec-len dif) 2)
        t
        nil)
    )
  )

(defun vec-len (vec)
  (sqrt (+ (pow (car vec) 2) (pow (cdr vec) 2)))
  )

(defun diff-vec (pos head-pos)
  (cons (- (car head-pos) (car pos)) (- (cdr head-pos) (cdr pos)))
  )

(defun vec-add (v1 v2)
  (cons (+ (car v1) (car v2)) (+ (cdr v1) (cdr v2)))
  )

(defun add-visited (pos lis)
  (if (not (member pos lis :test 'equal))
      (cons pos lis)
      lis
      )
  )

(defun repeat (inp num &optional (out nil))
  (if (> num 0)
      (repeat inp (1- num) (cons inp out))
      out
      )
  )

; from https://stackoverflow.com/questions/53575616/lisp-case-with-different-equality-predicate
(defmacro string-case (key &rest forms)
  (let ((k (gensym "KEY")))
    `(let ((,k ,key))
       (cond
         ,@(loop for (str . body) in forms
                 collect `((string= ,k ,str) ,@body))))))
